# Projet PHP : Site de vente avec authentification

## Contributeurs
- Ronan GUILBAULT
- Quentin MALLET

Conventions de nommage
Table: Pluriel minuscules
Model: UpperCamelcase singulier nomtable
Vue: dossier nom du Controller
controller: UpperCamelcase pluriel


par exemple: table auteurs
model: Auteur.php
vue: Auteurs/exampleAfficheAuteurs.ctp

controller:AuteursController.php
