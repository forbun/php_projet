all: clean projet  archive
	@echo -e '\e[32mArchive créée.\e[0m'

clean:
	@echo -e '\e[33mClear PROJET\e[0m'
	@rm -rf PROJET/

# A compléter, ne fait que passer le validateur
projet:
	@echo -e '\e[33mCreate PROJET folder\e[0m'
	@mkdir PROJET
	@cp  rapport/report.pdf PROJET/rapport.pdf
	@cp bd.sql PROJET/
	@mkdir PROJET/site
	@mkdir PROJET/site/app
	@mkdir PROJET/site/app/Controller
	@cp site/app/Controller/*.php PROJET/site/app/Controller
	@mkdir PROJET/site/app/Model
	@cp site/app/Model/*.php PROJET/site/app/Model
	@mkdir PROJET/site/app/View
	@cp -r site/app/View/Accueil PROJET/site/app/View
	@cp -r site/app/View/Admin PROJET/site/app/View
	@cp -r site/app/View/Connexion PROJET/site/app/View
	@cp -r site/app/View/Inscription PROJET/site/app/View
	@mkdir PROJET/site/app/View/Layouts
	@cp -r site/app/View/Layouts/default.ctp PROJET/site/app/View/Layouts
	@cp -r site/app/View/Panier PROJET/site/app/View
	@cp -r site/app/View/Produit PROJET/site/app/View
	@cp -r site/app/View/Profil PROJET/site/app/View
	@mkdir PROJET/site/app/webroot/
	@mkdir PROJET/site/app/webroot/img/
	@cp site/app/webroot/img/*_banner* PROJET/site/app/webroot/img
	@mkdir PROJET/site/app/webroot/css
	@cp site/app/webroot/css/cake.generic.css PROJET/site/app/webroot/css
	@mkdir PROJET/site/app/Config
	@cp site/app/Config/core.php PROJET/site/app/Config
	@cp site/app/Config/database.php PROJET/site/app/Config


archive:
	@echo -e '\e[33mCreate .tar archive\e[0m'
	@tar -cvf guilbault_mallet_projet.tar PROJET/
	@echo -e '\e[33mCompress with bzip2\e[0m'
	@bzip2 -f guilbault_mallet_projet.tar

verif:
	@./verif_archive.sh guilbault mallet

find-files:
	@git diff ebb928fa048c2dc5ec8febcb89e0551040e6197b --name-only # Commit 'Add Makefile', remove default files in CakePHP
