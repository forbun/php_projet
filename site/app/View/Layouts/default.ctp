<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css('cake.generic');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body>
<div id="header">
<?php
// Our banner, depends of connected or not, admin or not
if(AuthComponent::user() == null) {
    echo $this->Html->image("noauth_banner.jpg", array('alt' => "Non authentifié", 'height' => '200px'));
} else {
    if(AuthComponent::user('admin') != 1) {
        echo $this->Html->image("auth_banner.jpg", array('alt' => "Authentifié", 'height' => '200px'));
    } else {
        echo $this->Html->image("admin_banner.jpg", array('alt' => "Administrateur", 'height' => '200px'));
    }
}
?>
</div>
    <div id="container">
        <div id="content">
            <?php // All links ?>
            <nav>
            <ul>
                <?php
    echo "<li>" . $this->Html->link(
        'Accueil',
        '/accueil')
        ."</li>"
    ;
    echo "<br/>";
    if(AuthComponent::user() == null) {
        echo "<li>" . $this->Html->link(
            'Inscription',
            '/inscription/register')
            ."</li>"
        ;
        echo "<li>" . $this->Html->link(
            'Connexion',
            '/connexion/login')
            ."</li>"
        ;
    } else {
        echo "<li>" . $this->Html->link(
            'Les produits',
            '/produit')
            ."</li>"
        ;
        echo "<li>" . $this->Html->link(
            'Mon panier',
            '/panier')
            ."</li>"
        ;
        echo "<li>".$this->Html->link(
            'Mon profil',
            '/profil/getinfo')
            ."</li>"
        ;
        if(AuthComponent::user('admin') == 1) {
            echo "<br/>";
            echo "<li>" . $this->Html->link(
                'Administration',
                '/admin/membres')
                ."</li>"
            ;
        } else {
        }
        echo "<br/>";
        echo "<li>" . $this->Html->link(
            'Déconnexion',
            '/connexion/logout')
            ."</li>"
        ;
    }
                ?>
            </ul>
            </nav>

            <?php echo $this->Session->flash(); ?>

            <?php echo $this->fetch('content'); ?>
        </div>
        <div class="footer">
            <p>
                <?php echo $cakeVersion; ?>
            </p>
        </div>
    </div>
    <?php echo $this->element('sql_dump'); ?>
</body>
</html>
