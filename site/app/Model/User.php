<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
    public $hasOne = array(
        "Commande" => array(
        ),
        "Panier" => array(
        )
    );

    public $validate = array(
        'username' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'allowEmpty' => false,
            'message' => 'Merci d\'entrer un login valide !'
        ),
        'password' => array(
            'rule' => 'notEmpty',
            'required' =>  true,
            'allowEmpty' => false,
            'message' => 'Merci d\'entrer un mot de passe valide !'
        )
    );

    public function beforeSave($options = array()) {
        if(isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']);
        }
        return true;
    }
}
?>
