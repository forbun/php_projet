-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 31, 2016 at 04:02 PM
-- Server version: 5.6.28-0ubuntu0.15.10.1
-- PHP Version: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpprojet`
--

-- --------------------------------------------------------

--
-- Table structure for table `l31516_commandes`
--

CREATE TABLE IF NOT EXISTS `l31516_commandes` (
  `id` int(11) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `l31516_ligne_commandes`
--

CREATE TABLE IF NOT EXISTS `l31516_ligne_commandes` (
  `id` int(11) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  `commande_id` int(11) NOT NULL,
  `produit_id` int(11) NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `l31516_paniers`
--

CREATE TABLE IF NOT EXISTS `l31516_paniers` (
  `id` int(11) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `produit_id` int(11) NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `l31516_produits`
--

CREATE TABLE IF NOT EXISTS `l31516_produits` (
  `id` int(11) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  `libelle` varchar(255) CHARACTER SET latin1 NOT NULL,
  `prix` decimal(5,2) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `l31516_produits`
--

INSERT INTO `l31516_produits` (`id`, `created`, `modified`, `libelle`, `prix`, `stock`) VALUES
(1, '2016-03-21', '2016-03-21', 'banane', 2.24, 12),
(2, '2016-03-21', '2016-03-21', 'kiwi', 1.12, 4),
(3, '2016-03-21', '2016-03-21', 'pomme', 1.45, 0),
(4, '2016-03-21', '2016-03-21', 'raisin', 0.99, 123),
(5, '2016-03-21', '2016-03-21', 'fraise', 3.84, 21),
(6, '2016-03-21', '2016-03-21', 'cerise', 4.10, 12),
(7, '2016-03-21', '2016-03-21', 'orange', 2.00, 1),
(8, '2016-03-21', '2016-03-21', 'ananas', 5.12, 0),
(9, '2016-03-21', '2016-03-21', 'chou', 0.73, 125),
(10, '2016-03-21', '2016-03-21', 'tomate', 2.45, 12),
(11, '2016-03-21', '2016-03-21', 'carotte', 3.45, 51),
(12, '2016-03-21', '2016-03-21', 'artichaut', 1.21, 510),
(13, '2016-03-21', '2016-03-21', 'poivron', 3.21, 0),
(14, '2016-03-21', '2016-03-21', 'celeri', 1.56, 64),
(15, '2016-03-21', '2016-03-21', 'betterave', 0.99, 402),
(16, '2016-03-21', '2016-03-21', 'courgette', 1.24, 10);

-- --------------------------------------------------------

--
-- Table structure for table `l31516_users`
--

CREATE TABLE IF NOT EXISTS `l31516_users` (
  `id` int(11) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `l31516_users`
--

INSERT INTO `l31516_users` (`id`, `created`, `modified`, `username`, `password`, `admin`, `nom`) VALUES
(1, '2016-03-31', '2016-03-31', 'admin', '342ce5790a94743c1a25701c7a694ac300c85ed3', 1, 'Admin du Site'),
(2, '2016-03-31', '2016-03-31', 'gilles', 'f0011d3779f1c65fc10a736e241cdb34101a18d9', 0, 'Gilles'),
(3, '2016-03-31', '2016-03-31', 'patrice', 'b545c662fd857ca17b7e08ed45432cd20023e115', 0, 'Patrice');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `l31516_commandes`
--
ALTER TABLE `l31516_commandes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`user_id`) USING BTREE;

--
-- Indexes for table `l31516_ligne_commandes`
--
ALTER TABLE `l31516_ligne_commandes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commande_id` (`commande_id`) USING BTREE,
  ADD KEY `produit_id` (`produit_id`) USING BTREE;

--
-- Indexes for table `l31516_paniers`
--
ALTER TABLE `l31516_paniers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`user_id`) USING BTREE,
  ADD KEY `produit_id` (`produit_id`) USING BTREE;

--
-- Indexes for table `l31516_produits`
--
ALTER TABLE `l31516_produits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `l31516_users`
--
ALTER TABLE `l31516_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `l31516_commandes`
--
ALTER TABLE `l31516_commandes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `l31516_ligne_commandes`
--
ALTER TABLE `l31516_ligne_commandes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `l31516_paniers`
--
ALTER TABLE `l31516_paniers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `l31516_produits`
--
ALTER TABLE `l31516_produits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `l31516_users`
--
ALTER TABLE `l31516_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `l31516_commandes`
--
ALTER TABLE `l31516_commandes`
  ADD CONSTRAINT `l31516_commandes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `l31516_users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `l31516_ligne_commandes`
--
ALTER TABLE `l31516_ligne_commandes`
  ADD CONSTRAINT `l31516_ligne_commandes_ibfk_1` FOREIGN KEY (`commande_id`) REFERENCES `l31516_commandes` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `l31516_ligne_commandes_ibfk_2` FOREIGN KEY (`produit_id`) REFERENCES `l31516_produits` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `l31516_paniers`
--
ALTER TABLE `l31516_paniers`
  ADD CONSTRAINT `l31516_paniers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `l31516_users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `l31516_paniers_ibfk_2` FOREIGN KEY (`produit_id`) REFERENCES `l31516_produits` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
